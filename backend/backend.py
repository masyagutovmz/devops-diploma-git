"""System module."""
import xml.etree.ElementTree as ET
from datetime import datetime
from functions import get_first_last_day_month_with_format, download_file_from_url
from flask import Flask, json, jsonify
from flask_migrate import Migrate
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy import exc

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql://username:password@db/db?charset=utf8mb4'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
db = SQLAlchemy(app)
migrate = Migrate(app,db) #Initializing migrate.

class MyMetals(db.Model):
    """Metals docstring."""
    code = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(50), nullable=False)
    def __repr__(self):
        return f"<Metals {self.code}>"

class _Quotations(db.Model):
    """Quotations docstring."""
    id = db.Column(db.Integer, primary_key=True)
    date = db.Column(db.DateTime, default=datetime.utcnow())
    buy = db.Column(db.Float, nullable=False)
    sell = db.Column(db.Float, nullable=False)
    metals_code = db.Column(db.Integer, db.ForeignKey('metals.code'))
    def __repr__(self):
        return f"<Quotations {self.id}>"

db.create_all()

@app.route('/api/v1/update')
def update():
    """Update docstring."""
    dates = get_first_last_day_month_with_format(datetime.now())
    url = 'https://www.cbr.ru/scripts/xml_metall.asp?date_req1=' + dates[0] + '&date_req2='+dates[1]
    download_file_from_url(url,'xml_metall.xml')
    tree = ET.parse('xml_metall.xml')
    root = tree.getroot()
    for elem in root:
        for subelem in elem:
            if subelem.tag == 'Buy':
                buy = subelem.text
            else:
                sell = subelem.text
        temp_date = datetime.strptime(elem.attrib['Date'],"%d.%m.%Y")
        exists = _Quotations.query.filter_by(metals_code=elem.attrib['Code'],date=temp_date).first()
        if not exists:
            try:
                _metals = _Quotations(metals_code=int(elem.attrib['Code']),date=\
                    temp_date,buy=float(buy.replace(",", ".")),sell=float(sell.replace(",", ".")))
                db.session.add(_metals)
                db.session.commit()
            except exc.SQLAlchemyError:
                print("Failed to commit db!")
                return jsonify("Failed to commit db!")
    return jsonify("DB update successfully!")
@app.route('/api/v1/metals/<int:id>')
def metals_id(_id):
    """metals_id docstring."""
    quotations = _Quotations.query.filter_by(metals_code=_id).order_by(_Quotations.date.asc()).all()
    metals = MyMetals.query.filter_by(code=_id).first().name
    dict_metals = {}
    for elem in quotations:
        dict_metals[elem.id] = elem.date.strftime("%d.%m.%Y"),\
            elem.metals_code,metals,elem.buy,elem.sell
    return json.dumps(dict_metals)

@app.route('/api/v1/metals')
def get_metals():
    """metals docstring."""
    all_metals = MyMetals.query.order_by(MyMetals.name.asc()).all()
    dict_metals = {}
    for elem in all_metals:
        dict_metals[elem.code] = elem.name
    return json.dumps(dict_metals)
if __name__ == "__main__":
    from waitress import serve
    serve(app, host="0.0.0.0", port=5000)
