"""Backend_test module."""
import unittest
from datetime import datetime
#from backend import app
from functions import download_file_from_url
from functions import get_first_last_day_month_with_format

class BasicTestCase(unittest.TestCase):
    """BasicTestCase docstring."""
    def test_area(self):
        """test_area docstring."""
        self.assertEqual(get_first_last_day_month_with_format(datetime(2020, 5, 17),\
            '%d/%m/%Y'),('01/05/2020','31/05/2020'))
        self.assertEqual(get_first_last_day_month_with_format(datetime(2022, 1, 1),\
            '%d/%m/%Y'),('01/01/2022','31/01/2022'))
        self.assertEqual(get_first_last_day_month_with_format(datetime(2022, 12, 31),\
            '%d/%m/%Y'),('01/12/2022','31/12/2022'))
        self.assertEqual(get_first_last_day_month_with_format(datetime(2022, 2, 10),\
            '%d/%m/%Y'),('01/02/2022','28/02/2022'))
        self.assertEqual(download_file_from_url('https://mail.ru/favicon.ico','favicon.ico'),True)
    def test_values(self):
        """test_values docstring."""
        self.assertRaises(ValueError,download_file_from_url,'https://mail.ru/favicon.ico','favicon\
            faviconfaviconfaviconfaviconfaviconfaviconfaviconfaviconfaviconfaviconfavicon.ico')
        self.assertRaises(ValueError,download_file_from_url,'https://mailmailmailmailmailmailmail\
            mailmailmailmailmailmailmailmailmailmailmailmailmailmailmailmailmailmailmailmailmailmailmailmail\
            mailmailmailmailmailmailmailmailmailmailmailmailmailmailmailmailmailmailmailmailmailmailmailmail\
            mailmailmailmailmailmailmailmailmailmailmailmailmailmailmailmailmailmailmailmailmailmailmailmail\
            mailmailmailmailmailmailmailmailmailmailmailmailmailmailmailmailmailmailmailmailmailmailmailmail\
            mailmailmailmailmailmailmailmailmailmailmailmailmailmailmailmailmailmailmailmailmailmailmailmail\
            mailmailmailmailmailmailmailmailmailmailmailmailmailmailmailmailmailmailmailmailmailmailmailmail\
            mailmailmailmailmailmailmailmailmailmailmailmailmailmailmailmailmailmailmailmailmailmailmailmail\
            mailmailmailmailmailmailmailmailmailmailmailmailmailmailmailmailmailmailmailmailmailmailmailmail\
            mailmailmailmailmailmailmailmailmailmailmailmailmailmailmailmailmailmailmailmailmailmailmailmail\
            mailmailmailmailmailmailmailmailmailmailmailmailmailmailmailmailmailmailmailmailmailmailmailmail\
            mailmailmailmailmailmailmailmailmailmailmailmailmailmailmailmailmailmailmailmailmailmailmailmail\
            mailmailmailmailmailmailmailmailmailmailmailmailmailmailmailmailmailmailmailmailmailmailmailmail\
            mailmailmailmailmailmailmailmailmailmailmailmailmailmailmailmailmailmailmailmailmailmailmailmail\
            mailmailmailmailmailmailmailmailmailmailmailmailmailmailmailmailmailmailmailmailmailmailmailmail\
            mailmailmailmailmailmailmailmailmailmailmailmailmailmailmailmailmailmailmailmailmailmailmailmail\
            mailmailmailmailmailmailmailmailmailmailmailmailmailmailmailmailmailmailmailmailmailmailmailmail\
            mailmailmailmailmailmailmailmailmailmailmailmailmailmailmailmailmailmailmailmailmailmailmailmail\
            mailmailmailmailmailmailmailmailmailmailmailmailmailmailmailmailmailmailmailmailmailmailmailmail\
            mailmailmailmailmailmailmailmailmailmailmailmailmailmailmailmailmailmailmailmailmailmailmailmail\
            mailmailmailmailmailmailmailmailmailmailmailmailmailmailmailmailmailmailmailmailmailmailmailmail\
            mailmailmailmailmailmailmailmailmailmailmailmailmailmailmailmailmailmailmailmailmailmailmailmail\
            mailmailmailmailmailmailmailmailmailmailmailmailmailmailmailmailmailmailmailmailmailmailmailmail\
            mailmailmailmailmailmailmailmailmailmailmailmailmailmailmailmailmailmailmailmailmailmailmailmail\
            mail.ru/favicon.ico','favicon.ico')
    def test_types(self):
        """test_types docstring."""
        self.assertRaises(TypeError,get_first_last_day_month_with_format,1,'%d/%m/%Y')
        self.assertRaises(TypeError,get_first_last_day_month_with_format,datetime(2022, 1, 1),4)
        self.assertRaises(TypeError,download_file_from_url,1,'favicon.ico')
        self.assertRaises(TypeError,download_file_from_url,'https://mail.ru/favicon.ico',1)
    if __name__ == '__main__':
        unittest.main()
