"""Functions module."""
from calendar import monthrange
from datetime import datetime,timedelta
from requests import get,HTTPError

def get_first_last_day_month_with_format(_datetime,_format='%d/%m/%Y'):
    """get first and last days of now month with format (f) docstring."""
    if not isinstance(_datetime,datetime):
        raise TypeError("Dt must be datetime.datetime only")
    if not isinstance(_format,str):
        raise TypeError("F must be str only")
    first_day = (_datetime-timedelta(_datetime.day-1)).strftime(_format)
    last_day = (_datetime+timedelta(monthrange(_datetime.year, _datetime.month)[1] \
        - _datetime.day)).strftime(_format)
    return (first_day,last_day)

def download_file_from_url(url,filename):
    """download file from url and save with name filename docstring."""
    if not isinstance(url,str):
        raise TypeError("Url must be str only")
    if len(url) > 2048:
        raise ValueError("Lenght url must be no longer 2048 characters")
    if not isinstance(filename,str):
        raise TypeError("Url must be str only")
    if len(filename) > 30:
        raise ValueError("Lenght filename must be no longer 30 characters")
    try:
        response = get(url)
    except HTTPError:
        print("Problems with url!")
        return False
    try:
        with open(filename, 'wb') as file:
            file.write(response.content)
        file.close()
    except OSError:
        print("Could not open/read file!")
        return False
    return True
