"""System module."""
from urllib.error import URLError
from urllib.request import urlopen
import json
from flask import Flask, render_template

app = Flask(__name__)

@app.route('/',defaults={'_id': 1})
@app.route('/api/v1/metals/<int:_id>')
def metals_get(_id):
    """metals docstring."""
    url = "http://backend:5000/api/v1/metals/"+str(id)
    try:
#        response = urlopen(url)
        with urlopen(url) as response:
            data_json = json.loads(response.read())
    except URLError:
        print("Problems with url!")
        return render_template("error.html",mess="Failed to connect backend!")
    return render_template("metals.html",data_json=data_json)
@app.route('/api/v1/update')
def update_data():
    """update docstring."""
    url = "http://backend:5000/api/v1/update"
    try:
#        response = urlopen(url)
        with urlopen(url) as response:
            data_json = json.loads(response.read())
    except URLError:
        print("Problems with url!")
        return render_template("error.html",mess="Failed to connect to backend error!")
    return render_template("update.html",data_json=data_json)
#if __name__ == '__main__':
#    app.run(port=80)
if __name__ == "__main__":
    from waitress import serve
    serve(app, host="0.0.0.0", port=80)
